# The Real and simulated data matching
The scripts are built for matching the real-world signal positions and [NPMRDS](https://npmrds.ritis.org/analytics/) segments to the simulated network. 

## File description
- segment_matching: Find the closest distance between [NPMRDS](https://npmrds.ritis.org/analytics/) segments and simulated networks. 
- signal_matching: Find the closest distance between real-world signals and simulated intersections. 