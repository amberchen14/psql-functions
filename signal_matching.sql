CREATE OR REPLACE FUNCTION two_max(i integer, j bigint) RETURNS integer 
AS $$
        BEGIN
			if i>j then return i;
			else return j;
			end if;
        END;
$$ 
LANGUAGE plpgsql;



UPDATE signal_nodes SET link_count = two_max(link_count, (select count(*) from linkdetails where destination =nodeid));

--old
UPDATE signals_osm SET closest_node = b.nodeid, distance = subdist FROM (
	SELECT DISTINCT ON (b.nodeid) b.nodeid, ST_Distance(a.location, b.location) AS subdist 
		FROM signals_osm AS a, signal_nodes AS b
		ORDER BY b.nodeid, ST_Distance(a.location, b.location));
--new

drop table subdist;
select distinct signals_osm.osm_id, signal_nodes.nodeid, ST_Distance_Spheroid(signals_osm.location, signal_nodes.location, 'SPHEROID["WGS 84",6378137,298.257223563]') into subdist from signals_osm, signal_nodes;

drop table subdist;

drop table amber;
create table amber(osm_id bigserial, nodeid bigserial);
CREATE OR REPLACE FUNCTION bigtable() 
RETURNS VOID
AS $$
DECLARE @cnt int:=1;
DECLARE @bnt int :=1;
Declare @cend int:= 1; 
Declare @bend int:= 1; 
	BEGIN
	@cend := (select count(*) from signals_osm);
	@bend := (select count(*) from signal_nodes);	
	
	for cnt in 1 .. 10 loop
	for bnt in 1 .. 10 loop
		insert into amber (osm_id, nodeid) Values ((select osm_id from signals_osm order by osm_id limit 1 offset @cnt, nodeid from signal_nodes order by nodeid limit 1 offset @bnt));
	end loop;
	end loop;	
	END;	
$$ 
LANGUAGE plpgsql;


drop table amber;
create table amber(osm_id bigserial, nodeid bigserial, distance real);
CREATE OR REPLACE FUNCTION bigtable() 
RETURNS VOID
AS $$
DECLARE 
cnt record;
bnt record;
osm bigint:= 0;
node bigint:= 0;
dis real :=0;
	BEGIN	
	
	FOR cnt in select om_id from signals_osm LOOP
	for bnt in select nodeid from signal_nodes loop 
	osm := cnt.osm_id;
	node:= bnt.nodeid;
	dis := ST_Distance_Spheroid(cnt.location, bnt.location, 'SPHEROID["WGS 84",6378137,298.257223563]') 
	insert into amber (osm_id, nodeid) Values (osm, node, dis);
	end loop;
	end loop;	
	END;	
$$ 
LANGUAGE plpgsql;
select 1 from bigtable();

update amber set distance = ST_Distance_Spheroid(signals_osm.location, signal_nodes.location, 'SPHEROID["WGS 84",6378137,298.257223563]') from signal_nodes inner join signals_osm on amber.osm_id=signals_osm.osm_id where amber.nodeid = singal_nodes.nodeid;





select signals_osm.osm_id, ST_Distance_Spheroid(signals_osm.location, signal_nodes.location, 'SPHEROID["WGS 84",6378137,298.257223563]') , signal_nodes.nodeid into subdist from signals_osm, signal_nodes;
\copy subdist to '/home/ac59434/subdist.csv' DELIMITER ',' HEADER CSV

 order by signal_nodes.nodeid, ST_Distance_Spheroid(signals_osm.location, signal_nodes.location, 'SPHEROID["WGS 84",6378137,298.257223563]');
\copy subdist to '/home/ac59434/subdist.csv' DELIMITER ',' HEADER CSV


update signals_osm set closest_node = subdist.nodeid, distance = subdist.st_distance_spheroid from subdist where signals_osm.osm_id = subdist.osm_id;



drop table signals;
create table signals (id bigserial primary key, type integer, timeoffset integer);

--old

INSERT INTO signals SELECT DISTINCT ON (nodeid) nodeid AS id, 448 AS type, 0 AS timeoffset
	FROM signals_osm WHERE distance < 60 ORDER BY nodeid, distance;
--new

INSERT INTO signals SELECT DISTINCT signals_osm.osm_id AS id, 448 AS type, 0 AS timeoffset FROM signals_osm WHERE signals_osm.distance < 60 ORDER BY signals_osm.osm_id, signals_osm.distance;


INSERT INTO signals SELECT DISTINCT signals_osm.closest_node AS id, 448 AS type, 0 AS timeoffset FROM signals_osm WHERE signals_osm.distance < 60 ORDER BY signals_osm.osm_id, signals_osm.distance;


