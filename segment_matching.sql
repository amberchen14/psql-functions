DROP TABLE vista_geom;
select link as id into vista_geom from bus_route_link;
alter table vista_geom add column origin integer;
alter table vista_geom add column dest integer;
alter table vista_geom add column slon real;
alter table vista_geom add column slat real;
alter table vista_geom add column elon real;
alter table vista_geom add column elat real;
alter table vista_geom add column sgeom geometry;
alter table vista_geom add column egeom geometry;
alter table vista_geom add column geom geometry;

update vista_geom set origin=lk.source, dest=lk.destination from linkdetails lk where lk.id=vista_geom.id;
update vista_geom set slon=x, slat=y from nodes where nodes.id=vista_geom.origin;
update vista_geom set elon=x, elat=y from nodes where nodes.id=vista_geom.dest;
update vista_geom set sgeom=st_makepoint(slon, slat);
update vista_geom set egeom=st_makepoint(elon, elat);
update vista_geom set geom=st_makeline(sgeom, egeom);

drop table tmc_geom;
select tmc as id, slat, slon, elat, elon into tmc_geom from tmc_loc;
alter table tmc_geom add column sgeom  geometry;
alter table tmc_geom add column egeom  geometry;
update tmc_geom set sgeom=st_makepoint(slon, slat);
update tmc_geom set egeom=st_makepoint(elon, elat);

DROP TABLE tmc_contain;
select tmc as id into tmc_contain from tmc_loc;
alter table tmc_contain add column slink integer;
alter table tmc_contain add column elink integer;


DROP FUNCTION tmc_contain_func();
create or replace function tmc_contain_func()
returns void
as $$
declare
inrix_id  text;
inrix_sgeom geometry;
inrix_egeom geometry;

vista_id int:=0;
vista_geom geometry;
vista_slink int :=0;
vista_elink int :=0;

inrix record;
vista record;
begin
for inrix in select id from tmc_contain loop
	inrix_id :=inrix.id;
	inrix_sgeom := (select sgeom from tmc_geom where id=inrix_id);
	inrix_egeom :=(select egeom from tmc_geom where id=inrix_id);
 	
	for vista in select id from vista_geom loop
		vista_id := vista.id;
		vista_geom := (select geom from vista_geom where id = vista_id);
		if st_contains(vista_geom, inrix_sgeom) is true then
			vista_slink :=vista_id;
			EXIT;
		end if;
	end loop;

	for vista in select id from vista_geom loop
		vista_id := vista.id;
		vista_geom := (select geom from vista_geom where id = vista_id);	
		if st_contains(vista_geom, inrix_egeom) is true then
			vista_elink:=vista_id;
			EXIT;
		end if;
	end loop;  
    update tmc_contain set slink=vista_slink, elink=vista_elink where id=inrix_id;
	
end loop;
END;
$$
LANGUAGE plpgsql;
SELECT 1 FROM tmc_contain_func();


